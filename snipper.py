import glob
import json
from pathlib import Path

from kivy.app import App
from kivy import properties as kvprop
from kivy import clock
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.graphics import Line, InstructionGroup
from kivy.uix.checkbox import CheckBox
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget


class ImageProperties:

    def __init__(self, path):
        self.path = path
        self.reset()
        self.continuation = False
        self.validated = Path(self.json_path).exists()
        if self.validated:
            self.load()
        
    def set_crop(self, y):
        add = y not in self.crops
        if add:
            self.crops.append(y)
        else:
            self.crops.remove(y)
        self.crops.sort()
        self.validated = False
        return add

    def reset(self):
        self.crops = []

    @property
    def json_path(self):
        return self.path.replace(".jpg", ".json")

    def validate(self):
        with open(self.json_path, "w") as f:
            json.dump({"crops": self.crops, "continuation": self.continuation}, f)
        self.validated = True

    def load(self):
        data = json.load(open(self.json_path))
        self.crops = data["crops"]
        self.continuation = data["continuation"]


class ImageList:

    def __init__(self):
        pathes = sorted(glob.glob("../images/dico-fr-dz-*.jpg"))
        self.images = [ImageProperties(path) for path in pathes]
        self.pos = 0

    @property
    def current(self):
        return self.images[self.pos]

    def next(self):
        self.pos += 1

    def previous(self):
        self.pos -= 1

    def next_todo(self):
        try:
            self.pos = max(i for i, img in enumerate(self.images) if img.validated) + 1
        except ValueError:
            return 0

    def go_page(self, page):
        if page > len(self.images):
            raise IndexError("page out of range")
        self.pos = page


class PreviousButton(Button):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.previous_img()


class NextButton(Button):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.next_img()


class NextTodoButton(Button):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.next_todo_img()


class ValidateButton(Button):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.validate_crops()


class ResetButton(Button):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.reset_crops()


class PageInput(TextInput):
    img = kvprop.ObjectProperty(None)

    def on_text_validate(self):
        try:
            self.img.go_page(int(self.text))
        except ValueError:
            pass

class ContinuationCheckBox(CheckBox):
    img = kvprop.ObjectProperty(None)

    def on_press(self):
        self.img.set_continuation(self.active)


class ImageDisplay(ButtonBehavior, Image):

    cutter = kvprop.NumericProperty(50)
    page_input = kvprop.ObjectProperty(None)
    is_continuation = kvprop.ObjectProperty(None)
    image_snipper = kvprop.ObjectProperty(None)
    cuts = kvprop.DictProperty([])

    def __init__(self, **kwargs):
        self.image_list = ImageList()
        self.source = self.image_list.current.path
        super().__init__(**kwargs)
        clock.Clock.schedule_interval(self.update_cutter_pos, 1/60)
        # caches
        self._cut_lines = None
        self._validated_rect_color = None

    def next_img(self):
        self.image_list.next()
        self.update_image()

    def previous_img(self):
        self.image_list.previous()
        self.update_image()

    def next_todo_img(self):
        self.image_list.next_todo()
        self.update_image()

    def go_page(self, page):
        try:
            self.image_list.go_page(page)
            self.update_image()
        except IndexError:
            pass

    def get_page(self):
        return self.image_list.pos

    def reset_crops(self):
        self.image_list.current.reset()
        self.update_image(reset_crops=True)

    def validate_crops(self):
        image = self.image_list.current
        image.validate()
        self.update_image()

    def set_continuation(self, active):
        image = self.image_list.current
        image.continuation = active
        self.update_image()

    def update_cutter_pos(self, dt):
        self.cutter = self.image_snipper.parent.mouse_pos[1]

    @property
    def cut_lines(self):
        if self._cut_lines is None:
            self._cut_lines = self.canvas.get_group("lines")[0]
        return self._cut_lines

    @property
    def validated_rect_color(self):
        if self._validated_rect_color is None:
            self._validated_rect_color = self.canvas.get_group("validated_rect_color")[0]
        return self._validated_rect_color

    def validated_rect_visibility(self, visible):
        self.validated_rect_color.a = .4 if visible else 0

    def real_crop(self, display_crop):
        return round(float(display_crop) / self.height * self.texture.height)

    def display_crop(self, real_crop):
        return round(float(real_crop) * self.height / self.texture.height)

    def update_image(self,reset_crops=False):
        image = self.image_list.current
        if self.source != image.path:
            self.source = image.path
            reset_crops = True
        if reset_crops:
            self.cuts.clear()
            self.cut_lines.clear()
        self.validated_rect_visibility(image.validated)
        self.page_input.text = str(self.image_list.pos)
        self.is_continuation.active = image.continuation
        for crop in image.crops:
            display_crop = self.display_crop(crop)
            if crop not in self.cuts:
                self.cuts[crop] = Line(
                    points=(self.pos[0], display_crop, self.pos[0] + self.width, display_crop))
                self.cut_lines.add(self.cuts[crop])
        self.canvas.ask_update()

    def on_press(self):
        image = self.image_list.current
        crop = self.real_crop(self.image_snipper.parent.mouse_pos[1])
        added = image.set_crop(crop)
        self.update_image(reset_crops=not added)
        return False


class ImageSnipper(Widget):

    img = kvprop.ObjectProperty(None)
    validate_btn = kvprop.ObjectProperty(None)
    is_continuation = kvprop.ObjectProperty(None)

    def __init__(self, app):
        super().__init__()
        self.app = app


class SnipperApp(App):

    def build(self):
        self.snipper = ImageSnipper(app=self)
        return self.snipper


if __name__ == '__main__':
    snipper = SnipperApp()
    snipper.run()
