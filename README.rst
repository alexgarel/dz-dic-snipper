The aim of this project is to be able to cut images of an french to algerian dictionary.

Kivy makes it quite easy.

We place horizontal markers on a page. We also tell if this page is continuation of last page.
Finally we validate and it saves a json with this information, with same base name as images.
(we will later have a script to effectively snips images).

To dump the PDF, after trial/errors [#trials]_ ,
I did this, first I slash PDF in small part using::

   $ mkdir pdf
   $ pdfseparate dictionnaire\ français\ algérien\ abaluoft.pdf  pdf/dico-fr-dz-%03d.pdf

Then converting to images using imagemagick convert command, at density of 600 dpi::

   $ for f in pdf/*.pdf;do fname=${f#pdf/}; basename=${fname%.pdf}; convert -density 600x600 $f $basename.jpg;done

.. [#trials]
    - `pdfimages -list` shows jpx et jbig2 images.
    - `pdfimages -j -p` extracts them but only jb2e contains text
    - `jbig2dec -e -t png` gets jb2e images to png but definition is not good
